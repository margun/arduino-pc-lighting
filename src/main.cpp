#include <FastLED.h>
#include <EEPROM.h>  // импортируем библиотеку

#define LED_PIN 3
#define BRIGHTNESS 128
#define LED_TYPE WS2812
#define COLOR_ORDER GRB
#define SPEED 2
#define SCALE 30
#define COLOR_LOOP 1
#define BUTTON_PIN 2
#define DEFAULT_MODE 0
#define MODES_COUNT 7

// Params for width and height
const uint8_t kMatrixWidth = 10;
const uint8_t kMatrixHeight = 5;

// Param for different pixel layouts
const bool kMatrixSerpentineLayout = true;

#define NUM_LEDS (kMatrixWidth * kMatrixHeight)
#define MAX_DIMENSION ((kMatrixWidth > kMatrixHeight) ? kMatrixWidth : kMatrixHeight)

// The leds
CRGB leds[kMatrixWidth * kMatrixHeight];

// The 16 bit version of our coordinates
static uint16_t x;
static uint16_t y;
static uint16_t z;

// This is the array that we keep our computed noise values in
uint8_t noise[MAX_DIMENSION][MAX_DIMENSION];

int mode = 0;

void fillNoise8(uint16_t speed, uint16_t scale);
void mapNoiseToLEDsUsingPalette(uint8_t colorLoop, CRGBPalette16 currentPalette);
void ChangePaletteAndSettingsPeriodically();
uint16_t XY(uint8_t x, uint8_t y);
void ChangeModeByButton();
void one_color_all(int red, int green, int blue);

void setup()
{
  FastLED.addLeds<LED_TYPE, LED_PIN, COLOR_ORDER>(leds, NUM_LEDS);
  FastLED.setBrightness(BRIGHTNESS);
  pinMode(BUTTON_PIN, INPUT_PULLUP);

  // Initialize our coordinates to some random values
  x = random16();
  y = random16();
  z = random16();

  mode = EEPROM.read(0); 
}

void loop()
{
  if (mode == 0)
  {
    one_color_all(0, 0, 0);
  }
  else if (mode == 1)
  {
    fillNoise8(SPEED, SCALE);
    mapNoiseToLEDsUsingPalette(COLOR_LOOP, RainbowColors_p);
  }
  else if (mode == 2)
  {
    fillNoise8(SPEED, SCALE);
    mapNoiseToLEDsUsingPalette(COLOR_LOOP, ForestColors_p);
  }
  else if (mode == 3)
  {
    fillNoise8(SPEED, SCALE);
    mapNoiseToLEDsUsingPalette(COLOR_LOOP, CloudColors_p);
  }
  else if (mode == 4)
  {
    fillNoise8(SPEED, SCALE);
    mapNoiseToLEDsUsingPalette(COLOR_LOOP, LavaColors_p);
  }
  else if (mode == 5)
  {
    fillNoise8(SPEED, SCALE);
    mapNoiseToLEDsUsingPalette(COLOR_LOOP, OceanColors_p);
  }
  else if (mode == 6)
  {
    fillNoise8(SPEED, SCALE);
    mapNoiseToLEDsUsingPalette(COLOR_LOOP, PartyColors_p);
  }
  FastLED.show();
  ChangeModeByButton();
  delay(10);
}

// Fill the x/y array of 8-bit noise values using the inoise8 function.
void fillNoise8(uint16_t speed, uint16_t scale)
{
  // If we're running at a low "speed", some 8-bit artifacts become visible
  // from frame-to-frame.  In order to reduce this, we can do some fast data-smoothing.
  // The amount of data smoothing we're doing depends on "speed".
  uint8_t dataSmoothing = 0;
  if (speed < 50)
  {
    dataSmoothing = 200 - (speed * 4);
  }

  for (int i = 0; i < MAX_DIMENSION; i++)
  {
    int ioffset = scale * i;
    for (int j = 0; j < MAX_DIMENSION; j++)
    {
      int joffset = scale * j;

      uint8_t data = inoise8(x + ioffset, y + joffset, z);

      // The range of the inoise8 function is roughly 16-238.
      // These two operations expand those values out to roughly 0..255
      // You can comment them out if you want the raw noise data.
      data = qsub8(data, 16);
      data = qadd8(data, scale8(data, 39));

      if (dataSmoothing)
      {
        uint8_t oldData = noise[i][j];
        uint8_t newData = scale8(oldData, dataSmoothing) + scale8(data, 256 - dataSmoothing);
        data = newData;
      }

      noise[i][j] = data;
    }
  }

  z += speed;

  // apply slow drift to X and Y, just for visual variation.
  x += speed / 8;
  y -= speed / 16;
}

void mapNoiseToLEDsUsingPalette(uint8_t colorLoop, CRGBPalette16 currentPalette)
{
  static uint8_t ihue = 0;

  for (int i = 0; i < kMatrixWidth; i++)
  {
    for (int j = 0; j < kMatrixHeight; j++)
    {
      // We use the value at the (i,j) coordinate in the noise
      // array for our brightness, and the flipped value from (j,i)
      // for our pixel's index into the color palette.

      uint8_t index = noise[j][i];
      uint8_t bri = noise[i][j];

      // if this palette is a 'loop', add a slowly-changing base value
      if (colorLoop)
      {
        index += ihue;
      }

      // brighten up, as the color palette itself often contains the
      // light/dark dynamic range desired
      if (bri > 127)
      {
        bri = 255;
      }
      else
      {
        bri = dim8_raw(bri * 2);
      }

      CRGB color = ColorFromPalette(currentPalette, index, bri);
      leds[XY(i, j)] = color;
    }
  }

  ihue += 1;
}

//
// Mark's xy coordinate mapping code.  See the XYMatrix for more information on it.
//
uint16_t XY(uint8_t x, uint8_t y)
{
  uint16_t i;
  if (kMatrixSerpentineLayout == false)
  {
    i = (y * kMatrixWidth) + x;
  }
  if (kMatrixSerpentineLayout == true)
  {
    if (y & 0x01)
    {
      // Odd rows run backwards
      uint8_t reverseX = (kMatrixWidth - 1) - x;
      i = (y * kMatrixWidth) + reverseX;
    }
    else
    {
      // Even rows run forwards
      i = (y * kMatrixWidth) + x;
    }
  }
  return i;
}

void ChangeModeByButton() {
  static int previousValue = 1;
  int currentValue = digitalRead(BUTTON_PIN);

  if (previousValue == 1 && currentValue == 0) {
    mode = (mode + 1) % MODES_COUNT;
    EEPROM.update(0, mode); 
  }

  previousValue = currentValue;
}

void one_color_all(int red, int green, int blue) {
  for (int i = 0 ; i < NUM_LEDS; i++ ) {
    leds[i].setRGB( red, green, blue);
  }
}